package com.michalrys.user_service;

public class HelpTips {
    // Zadanie:
    //User Service metody:
    //1. void add(login, name, lastName)
    //2. ??? find(login)
    //3. void update(login, name)
    //4. void update(login, lastName)
    //5. void delete(login)
    //6. Add with the login that exists => throw an Exception
    //7. Find, Update, Delete when login does not exist => throw an Exception
    //8. Validate login before add.
    //9. Login is unique
    //10. Persist (store them somewhere) information
    //Dla tych co mają za dużo wolnego czasu:
    //Jak spełnić wymagania w taki sposób aby:
    //1. Nie dodać więcej niż wymagane.
    //2. Umożliwić sobie prosty rozwój w przyszłości, gdy ilość danych będzie się zmieniać.
    //
    //PODPOWIEDŹ Z TABLICY:
    //public class UserSevice {
    //    /**
    //     * - validate login
    //     * - throws exception if user with login exists
    //     * - create user
    //     * - store user
    //     */
    //    void add(boolean login, boolean name, boolean lastName) {/*...*/}
    //
    //    /**
    //     * - throws exception if user does not exist
    //     * - return ???
    //     */
    //    int find(boolean login) {/*...*/}
    //
    //    /**
    //     * - throws exception if user does not exist
    //     * - update user with new information
    //     * - store user
    //     */
    //    void update(boolean login, boolean name) {/*...*/}
    //    void update(boolean login, int lastname) {/*...*/}
    //
    //    /**
    //     * - throws exception if user deas not exist
    //     * - remove user from the storage
    //     */
    //    void delete(boolean login) {/*...*/}
    //}
}
