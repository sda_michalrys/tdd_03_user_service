package com.michalrys.user_service;

public class LoginIsNotUniqueException extends RuntimeException {
    public LoginIsNotUniqueException(Login login) {
        super("Given login is not unique: " + login);
    }
}