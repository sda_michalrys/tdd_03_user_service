package com.michalrys.user_service;

import com.michalrys.user_service.data.AbstractDataToStore;
import com.michalrys.user_service.data.LastName;
import com.michalrys.user_service.data.Name;

import java.util.HashMap;
import java.util.Map;

class UserService {

    private final LoginValidator loginValidator;
    private final Map<Login, Map<String, AbstractDataToStore>> userServiceContainer;

    UserService(LoginValidator loginValidator, Map<Login, Map<String, AbstractDataToStore>> userServiceContainer) {
        this.loginValidator = loginValidator;
        this.userServiceContainer = userServiceContainer;
        // this.userServiceContainer = new HashMap<>(); - don't do this, because then it's not possible to check storage
    }

    boolean find(Login login) {
        if (loginValidator.isValid(login) && userServiceContainer.containsKey(login)) {
            // this data could be a return value
            // Map<String, AbstractDataToStore> dataForLogin = userServiceContainer.get(login);
            return true;
        } else {
            throw new LoginNotFoundException(login);
        }
    }

    void add(Login login, Name name, LastName lastName) {
        if (userServiceContainer.containsKey(login)) {
            throw new LoginIsNotUniqueException(login);
        }
        userServiceContainer.put(login, new HashMap<>());
        userServiceContainer.get(login).put(Name.class.getSimpleName(), name);
        userServiceContainer.get(login).put(LastName.class.getSimpleName(), lastName);
    }

    void update(Login login, Name name) {
        if (!userServiceContainer.containsKey(login)) {
            throw new LoginNotFoundException(login);
        }
        userServiceContainer.get(login).put(Name.class.getSimpleName(), name);

    }

    void update(Login login, LastName lastName) {
        if (!userServiceContainer.containsKey(login)) {
            throw new LoginNotFoundException(login);
        }
        userServiceContainer.get(login).put(LastName.class.getSimpleName(), lastName);
    }

    public void delete(Login login) {
        if (!userServiceContainer.containsKey(login)) {
            throw new LoginNotFoundException(login);
        }
        userServiceContainer.remove(login);
    }
}
