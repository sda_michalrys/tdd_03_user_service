package com.michalrys.user_service.data;

public class LastName extends AbstractDataToStore {
    private final String lastName;

    public LastName(String name) {
        this.lastName = name;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LastName lastName = (LastName) o;

        return lastName != null ? lastName.equals(lastName.lastName) : lastName.lastName == null;
    }

    @Override
    public int hashCode() {
        return lastName != null ? lastName.hashCode() : 0;
    }

    @Override
    public String toString() {
        return lastName;
    }
}
