package com.michalrys.user_service;

public class LoginNotFoundException extends RuntimeException {
    public LoginNotFoundException(Login login) {
        super("Given login does not exist: " + login.toString());

    }
}
