package com.michalrys.user_service;

import org.junit.Assert;
import org.junit.Test;

public class LoginNotFoundExceptionTest {

    @Test
    public void shouldGiveMessageWithSignificantDetails() {
        // given
        Login login = new Login("MarioBros");
        LoginNotFoundException loginNotFoundException = new LoginNotFoundException(login);

        // when
        String exceptionMessage = loginNotFoundException.getMessage();

        // then
        Assert.assertEquals("Given login does not exist: MarioBros", exceptionMessage);
    }
}