package com.michalrys.user_service;

import com.michalrys.user_service.data.AbstractDataToStore;
import com.michalrys.user_service.data.LastName;
import com.michalrys.user_service.data.Name;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import java.util.HashMap;
import java.util.Map;

public class UserServiceTest {
    @Test(expected = LoginNotFoundException.class)
    public void shouldNotFindLogin() {
        // given login - must be validated before add
        LoginValidator loginValidator = Mockito.mock(LoginValidator.class);
        BDDMockito.given(loginValidator.isValid(ArgumentMatchers.any())).willReturn(true);

        // given login and user
        Login loginToFind = new Login("TubularBell");
        Login loginDifferentInDatabase = new Login("TubularBell2");
        Name name = new Name("Mike");
        LastName lastName = new LastName("Oldfield");

        // data put into userService must persist somewhere - hashmap is used
        Map<Login, Map<String, AbstractDataToStore>> userServiceContainer = new HashMap<>();

        // given userService
        UserService userService = new UserService(loginValidator, userServiceContainer);
        userService.add(loginDifferentInDatabase, name, lastName);

        // when
        boolean resultOfFindingLogin = userService.find(loginToFind);

        // then
        // throw exception
    }

    @Test
    public void shouldAddLoginNameLastNameWhenLoginIsUniqueAndCorrect() {
        // given login - must be validated before add
        LoginValidator loginValidator = Mockito.mock(LoginValidator.class);
        BDDMockito.given(loginValidator.isValid(ArgumentMatchers.any())).willReturn(true);

        // given login and user
        Login login = new Login("TubularBell");
        Name name = new Name("Mike");
        LastName lastName = new LastName("Oldfield");

        // data put into userService must persist somewhere - hashmap is used
        Map<Login, Map<String, AbstractDataToStore>>  userServiceContainer = new HashMap<>();

        // given userService
        UserService userService = new UserService(loginValidator, userServiceContainer);

        // when
        userService.add(login, name, lastName);

        // then
        Assert.assertTrue(userServiceContainer.containsKey(login));
        Assert.assertTrue(userServiceContainer.get(login).containsValue(name));
        Assert.assertTrue(userServiceContainer.get(login).containsValue(lastName));
    }

    @Test
    public void shouldFindLoginWhenLoginExistsInDatabase() {
        // given login - must be validated before add
        LoginValidator loginValidator = Mockito.mock(LoginValidator.class);
        BDDMockito.given(loginValidator.isValid(ArgumentMatchers.any())).willReturn(true);

        // given login and user
        Login login = new Login("TubularBell");
        Name name = new Name("Mike");
        LastName lastName = new LastName("Oldfield");

        // data put into userService must persist somewhere - hashmap is used
        Map<Login, Map<String, AbstractDataToStore>> userServiceContainer = new HashMap<>();

        // given userService
        UserService userService = new UserService(loginValidator, userServiceContainer);
        userService.add(login, name, lastName);

        // when
        boolean resultOfFindingLogin = userService.find(login);

        // then
        Assert.assertTrue(resultOfFindingLogin);
    }

    @Test(expected = LoginIsNotUniqueException.class)
    public void shouldNotAddLoginNameLastNameWhenLoginIsNotUnique() {
        // given login - must be validated before add
        LoginValidator loginValidator = new LoginValidator();

        // given login and user
        Login login = new Login("TubularBell");
        Name name = new Name("Mike");
        LastName lastName = new LastName("Oldfield");

        // data put into userService must persist somewhere - hashmap is used
        Map<Login, Map<String, AbstractDataToStore>>  userServiceContainer = new HashMap<>();

        // given userService
        UserService userService = new UserService(loginValidator, userServiceContainer);

        // given - add login first time
        userService.add(login, name, lastName);

        // when - add the same login
        userService.add(login, name, lastName);

        // then
        // throw exception
    }

    @Test(expected = LoginNotFoundException.class)
    public void shouldNotUpdateNameWhenLoginDoesNotExistsInDatabase() {
        // given login - must be validated before add
        LoginValidator loginValidator = new LoginValidator();

        // given login and user
        Login login = new Login("TubularBell");
        Login loginDifferentInDatabase = new Login("TubularBell2");
        Name name = new Name("Mike");
        LastName lastName = new LastName("Oldfield");

        // data put into userService must persist somewhere - hashmap is used
        Map<Login, Map<String, AbstractDataToStore>> userServiceContainer = new HashMap<>();

        // given userService
        UserService userService = new UserService(loginValidator, userServiceContainer);
        userService.add(loginDifferentInDatabase, name, lastName);

        // when
        userService.update(login, name);

        // then
        // throw exception
    }

    @Test
    public void shouldUpdateNameOnlyWhenLoginExistsInDatabase() {
        // given login - must be validated before add
        LoginValidator loginValidator = Mockito.mock(LoginValidator.class);
        BDDMockito.given(loginValidator.isValid(ArgumentMatchers.any())).willReturn(true);

        // given login and user
        Login login = new Login("TubularBell");
        Name name = new Name("Mike");
        Name nameUpdateValue = new Name("Michal");
        LastName lastName = new LastName("Oldfield");

        // data put into userService must persist somewhere - hashmap is used
        Map<Login, Map<String, AbstractDataToStore>>  userServiceContainer = new HashMap<>();

        // given userService
        UserService userService = new UserService(loginValidator, userServiceContainer);
        userService.add(login, name, lastName);

        // when
        userService.update(login, nameUpdateValue);

        // then
        Assert.assertEquals(nameUpdateValue, userServiceContainer.get(login).get(Name.class.getSimpleName()));
        Assert.assertEquals(lastName, userServiceContainer.get(login).get(LastName.class.getSimpleName()));
    }

    @Test
    public void shouldUpdateLastNameOnlyWhenLoginExistsInDatabase() {
        // given login - must be validated before add
        LoginValidator loginValidator = Mockito.mock(LoginValidator.class);
        BDDMockito.given(loginValidator.isValid(ArgumentMatchers.any())).willReturn(true);

        // given login and user
        Login login = new Login("TubularBell");
        Name name = new Name("Mike");
        LastName lastName = new LastName("Oldfield");
        LastName lastNameUpdateValue = new LastName("Staropolski");

        // data put into userService must persist somewhere - hashmap is used
        Map<Login, Map<String, AbstractDataToStore>>  userServiceContainer = new HashMap<>();

        // given userService
        UserService userService = new UserService(loginValidator, userServiceContainer);
        userService.add(login, name, lastName);

        // when
        userService.update(login, lastNameUpdateValue);

        // then
        Assert.assertEquals(name, userServiceContainer.get(login).get(Name.class.getSimpleName()));
        Assert.assertEquals(lastNameUpdateValue, userServiceContainer.get(login).get(LastName.class.getSimpleName()));
    }

    @Test(expected = LoginNotFoundException.class)
    public void shouldNotDeleteStoredDataByAGivenLoginWhenLoginDoesNotExist() {
        // given login - must be validated before add
        LoginValidator loginValidator = Mockito.mock(LoginValidator.class);
        BDDMockito.given(loginValidator.isValid(ArgumentMatchers.any())).willReturn(true);

        // given login and user
        Login login = new Login("TubularBell");
        Login loginOtherInDatabase = new Login("TubularBell2");
        Name name = new Name("Mike");
        LastName lastName = new LastName("Oldfield");

        // data put into userService must persist somewhere - hashmap is used
        Map<Login, Map<String, AbstractDataToStore>>  userServiceContainer = new HashMap<>();

        // given userService
        UserService userService = new UserService(loginValidator, userServiceContainer);
        userService.add(loginOtherInDatabase, name, lastName);

        // when
        userService.delete(login);

        // then
        // throw exception
    }

    @Test
    public void shouldDeleteStoredDataByAGivenLoginWhenLoginExists() {
        // given login - must be validated before add
        LoginValidator loginValidator = Mockito.mock(LoginValidator.class);
        BDDMockito.given(loginValidator.isValid(ArgumentMatchers.any())).willReturn(true);

        // given login and user
        Login login = new Login("TubularBell");
        Login loginOtherInDatabase = new Login("TubularBell2");
        Name name = new Name("Mike");
        LastName lastName = new LastName("Oldfield");

        // data put into userService must persist somewhere - hashmap is used
        Map<Login, Map<String, AbstractDataToStore>>  userServiceContainer = new HashMap<>();

        // given userService
        UserService userService = new UserService(loginValidator, userServiceContainer);
        userService.add(login, name, lastName);
        userService.add(loginOtherInDatabase, name, lastName);

        // when
        userService.delete(login);

        // then
        Assert.assertFalse(userServiceContainer.containsKey(login));
    }
}