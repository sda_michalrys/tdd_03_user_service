package com.michalrys.user_service;

import org.junit.Assert;
import org.junit.Test;

public class LoginIsNotUniqueExceptionTest {
    @Test
    public void shouldReturnMessageWithWrongLogin() {
        // given
        Login login = new Login("TestLogin");
        LoginIsNotUniqueException loginIsNotUniqueException = new LoginIsNotUniqueException(login);

        // when
        String errorMessage = loginIsNotUniqueException.getMessage();

        // then
        Assert.assertEquals("Given login is not unique: TestLogin", errorMessage);
    }
}